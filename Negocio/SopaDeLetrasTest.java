package Negocio;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class SopaDeLetrasTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class SopaDeLetrasTest
{
    /**
     * Default constructor for test class SopaDeLetrasTest
     */
    public SopaDeLetrasTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }

    @Test
    public void crearMatriz() throws Exception
    {
        String palabras="marc,pe,a";
        SopaDeLetras s=new SopaDeLetras(palabras);

        char ideal[][]={{'m','a','r','c'},{'p','e'},{'a'}};
        char esperado[][]=s.getSopas();
        // :) --> ideal == esperado

        boolean pasoPrueba=sonIguales(ideal, esperado);
        assertEquals(true,pasoPrueba);

    }

    @Test
    public void crearMatriz_conError() throws Exception
    {
        String palabras="marc,pe,a";
        SopaDeLetras s=new SopaDeLetras();

        char ideal[][]={{'m','a','r','c'},{'p','e'},{'a'}};
        char esperado[][]=s.getSopas();
        // :) --> ideal == esperado

        boolean pasoPrueba=sonIguales(ideal, esperado);
        assertEquals(false,pasoPrueba);

    }

    private boolean sonIguales(char m1[][], char m2[][])
    {

        if(m1==null || m2==null || m1.length !=m2.length)
            return false; 

        int cantFilas=m1.length; 

        for(int i=0;i<cantFilas;i++)
        {
            if( !sonIgualesVector(m1[i],m2[i]))
                return false;
        }

        return true;
    }

    private boolean sonIgualesVector(char v1[], char v2[])
    {
        if(v1.length != v2.length)
            return false;

        for(int j=0;j<v1.length;j++)
        {
            if(v1[j]!=v2[j])
                return false;
        }
        return true;
    }

    //VALIDACION DE MATRIZ CUADRADA
    @Test
    public void esCuadrada() throws Exception{
        
        SopaDeLetras s = new SopaDeLetras("casa,cara,mama,dedo");
        boolean esperado = s.esCuadrada();
        
        assertEquals(true, esperado);
    }
    
    @Test 
    public void noEsCuadrada()throws Exception{
        SopaDeLetras s = new SopaDeLetras("casa,carnada,mama,dedo");
        boolean esperado = s.esCuadrada();
        
        assertEquals(false, esperado);
    
    }
    
    @Test 
    public void noEsCuadrada2()throws Exception{
        SopaDeLetras s = new SopaDeLetras("casa");
        boolean esperado = s.esCuadrada();
        
        assertEquals(false, esperado);
    
    }
    
    @Test
    public void esCuadrada2()throws Exception{
        
        SopaDeLetras s = new SopaDeLetras("casas,caras,mamas,dedos,manos");
        boolean esperado = s.esCuadrada();
        
        assertEquals(true, esperado);
    }
    
    //CUANTAS VECES SE REPITE
    @Test
    public void getContar()throws Exception{
        
        SopaDeLetras s = new SopaDeLetras("manno,pief,rodi,cabe");
        int esperado = s.getContar("ma");
        int ideal = 1;
        boolean rta = esperado==ideal;
        
        assertEquals(true, rta);
    }
    
    @Test
    public void getContarVacio()throws Exception{
        
        SopaDeLetras s = new SopaDeLetras("mama,papa");
        int esperado = s.getContar("");
        int ideal = 0;
        boolean rta = esperado==ideal;
        
        assertEquals(true, rta);
    }
    
    @Test
    public void getContarVacio2()throws Exception{
        
        SopaDeLetras s = new SopaDeLetras();
        int esperado = s.getContar("ma");
        int ideal = 0;
        
        boolean rta = esperado==ideal;
        
        assertEquals(true, rta);
    }
    
    @Test
    public void getContar1()throws Exception{
        
        SopaDeLetras s = new SopaDeLetras();
        int esperado = s.getContar("ma");
        int ideal = 0;
        
        boolean rta = esperado==ideal;
        
        assertEquals(true, rta);
    }
    
     @Test
    public void getContar3()throws Exception{
        
        SopaDeLetras s = new SopaDeLetras("jose,londres");
        int esperado = s.getContar("e");
        int ideal = 2;
        
        boolean rta = esperado==ideal;
        
        assertEquals(true, rta);
    }
    
     @Test
    public void getContar4()throws Exception{
        
        SopaDeLetras s = new SopaDeLetras("jose,londres,lola,pepe");
        int esperado = s.getContar("joseandres");
        int ideal = 0;
        
        
        assertEquals(ideal, esperado);
    }
    
    //VALIDACION DE MATRIZ DISPERSA
    @Test
    public void esDispersa()throws Exception{
        
        SopaDeLetras s = new SopaDeLetras("manno,pief,rodi,cabe");
        boolean esperado = s.esDispersa();
        
        assertEquals(true, esperado);
        
    }
    
    @Test
    public void esDispersa2()throws Exception{
        
        SopaDeLetras s = new SopaDeLetras("manno,picef,rodi,cabe");
        boolean esperado = s.esDispersa();
        
        assertEquals(true, esperado);
        
    }
    
    @Test
    public void noEsDispersa()throws Exception{
        
        SopaDeLetras s = new SopaDeLetras("mano,pief,rodi,cabe");
        boolean esperado = s.esDispersa();
        
        assertEquals(false, esperado);
       
    }
    
    @Test
    public void noEsDispersa2()throws Exception{
        
        SopaDeLetras s = new SopaDeLetras("mano,pief,rodi,caben");
        boolean esperado = s.esDispersa();
        
        assertEquals(true, esperado);
       
    }
    
    //VALIDAR SI ES RECTANGULAR 
    @Test 
    public void esRectangular()throws Exception{
    
        SopaDeLetras s = new SopaDeLetras("manoe,piefe,rodie,caben,fhgjf,casar");
        boolean esperado = s.esRectangular();
        
        assertEquals(true, esperado);
        
    }
    
    @Test 
    public void esRectangular2()throws Exception{
    
        SopaDeLetras s = new SopaDeLetras("manoe,piefe,rodie,caben,coser,casar");
        boolean esperado = s.esRectangular();
        
        assertEquals(true, esperado);
    }
    
    @Test 
    public void noEsRectangular()throws Exception{
    
        SopaDeLetras s = new SopaDeLetras("manoe,piefe,rodie,caben,coser");
        boolean esperado = s.esRectangular();
        
        assertEquals(false, esperado);
    }
    
    @Test 
    public void noEsRectangular2()throws Exception{
    
        SopaDeLetras s = new SopaDeLetras("manos,nm");
        boolean esperado = s.esRectangular();
        
        assertEquals(false, esperado);
    }
    
    //GET DIAGONAL PRINCIPAL
    @Test
    public void getDiagonalPrincipal()throws Exception{
        
        SopaDeLetras s = new SopaDeLetras("era,car,ola");
        char esperado[] = s.getDiagonalPrincipal();
        char ideal[] = {'e','a','a'};
        
        boolean esIgual = sonIgualesVector(esperado,ideal);
        
        assertEquals(true, esIgual);
    
    }
    
    @Test
    public void getDiagonalPrincipal2()throws Exception{
        
        SopaDeLetras s = new SopaDeLetras("casa,caro,hola,lado");
        char esperado[] = s.getDiagonalPrincipal();
        char ideal[] = {'c','a','l','o'};
        
        boolean esIgual = sonIgualesVector(esperado,ideal);
        
        assertEquals(true, esIgual);
    
    }
    
    @Test
    public void getDiagonalPrincipal_diferente()throws Exception{
        
        SopaDeLetras s = new SopaDeLetras("casa,caro,hola,lado");
        char esperado[] = s.getDiagonalPrincipal();
        char ideal[] = {'c','m','l','o'};
        
        boolean esIgual = sonIgualesVector(esperado,ideal);
        
        assertEquals(false, esIgual);
    }
    
    @Test
    public void getDiagonalPrincipal_Error()throws Exception{
        
        SopaDeLetras s = new SopaDeLetras("casa,caro");
        char esperado[] = s.getDiagonalPrincipal();
        char ideal[] = null;
        
        assertEquals(null, ideal);
    }
    
    @Test
    public void getDiagonalPrincipal_soloUna()throws Exception{
        
        SopaDeLetras s = new SopaDeLetras("m");
        char esperado[] = s.getDiagonalPrincipal();
        char ideal[] = {'m'};
        boolean esIgual = sonIgualesVector(esperado,ideal);
        
        assertEquals(true, esIgual);
    }
    /** ****************************************************************************************** **/
    
    @Test
    public void esCuadradaVacio()throws Exception{
        
        SopaDeLetras s = new SopaDeLetras();
        boolean esperado = s.esCuadrada();
        
        assertEquals(false,esperado);
        
    }
    
    @Test
    public void esDispersaVacio()throws Exception{
        
        SopaDeLetras s = new SopaDeLetras();
        boolean esperado = s.esDispersa();
        assertEquals(false,esperado);
        
    }
    
    @Test
    public void esRectangularVacio()throws Exception{
        
        SopaDeLetras s = new SopaDeLetras();
        boolean esperado = s.esRectangular();
        assertEquals(false,esperado);
        
    }
    
}
