package Negocio;

/**
 * Write a description of class SopaDeLetras here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SopaDeLetras
{
    // instance variables - replace the example below with your own
    private char sopas[][];
    /**
     * Constructor for objects of class SopaDeLetras
     */
    public SopaDeLetras()
    {

    }

    public SopaDeLetras(String palabras) throws Exception
    {
        if(palabras==null || palabras.isEmpty())
        {
            throw new Exception("Error no se puede crear la matriz de char para la sopa de letras");
        }

        //Crear la matriz con las correspondientes filas:
        String palabras2[]=palabras.split(",");
        this.sopas=new char[palabras2.length][];
        int i=0;
        for(String palabraX:palabras2)
        {
            //Creando las columnas de la fila i
            this.sopas[i]=new char[palabraX.length()];
            pasar(palabraX,this.sopas[i]);
            i++;
        }

    }

    private void pasar (String palabra, char fila[])
    {

        for(int j=0;j<palabra.length();j++)
        {
            fila[j]=palabra.charAt(j);
        }

    }

    public String toString()
    {
        String msg="";
        for(int i=0;i<this.sopas.length;i++)
        {
            for (int j=0;j<this.sopas[i].length;j++)
            {
                msg+=this.sopas[i][j]+"\t";
            }

            msg+="\n";

        }
        return msg;
    }

    public String toString2()
    {
        String msg="";
        for(char filas[]:this.sopas)
        {
            for (char dato :filas)
            {
                msg+=dato+"\t";
            }

            msg+="\n";

        }
        return msg;
    }

    public String toString3()throws Exception{
        String str="";
        if(!esCuadrada())
            throw new Exception("No es cuadrada");

        for(char letra : getDiagonalPrincipal())
            str+=letra;    
        return str;
    }

    public boolean esCuadrada() 
    {  
        boolean rta=true;
        if(getSopas()==null){return false;}
        for(int i = 0; i<this.sopas.length && rta; i++){
            if(this.sopas.length!=this.sopas[i].length){
                rta = false;
            }
        }

        return rta;
    }

    public boolean esDispersa() 
    {
        if(getSopas()==null){return false;}
        boolean rta=!esCuadrada()&&!esRectangular();
        return rta;
    }

    public boolean esRectangular() 
    {
        boolean esRectangular =true;
        if(getSopas()==null){return false;}
        for(int i = 0; i<this.sopas.length-1 && esRectangular; i++){
            if((this.sopas[i].length!=this.sopas[i+1].length) || esCuadrada()){
                esRectangular=false;
            }
        }

        return esRectangular;
    }

    /*
    retorna cuantas veces esta la palabra en la matriz
     */
    public int getContar(String palabra)throws Exception
    {
        int contador = 0;     String palabraAuxiliar = "";

        if(palabra==null||palabra.isEmpty() || this.sopas == null)  {
            return 0;
        }

        for(int i = 0; i<this.sopas.length; i++){
            if(palabra.length()<=sopas[i].length){
                for(int j = 0; j<this.sopas[i].length; j++){
                    palabraAuxiliar = palabraAuxiliar + this.sopas[i][j];

                    if(palabraAuxiliar.charAt(0) != palabra.charAt(0))
                        palabraAuxiliar = "";

                    if(palabraAuxiliar.equals(palabra)){
                        contador++;
                        palabraAuxiliar = "";
                    }
                }  
            }
        }
        return contador;
    }

    /*
    debe ser cuadrada sopas
     */
    public char []getDiagonalPrincipal() throws Exception
    {
        if(!esCuadrada() || this.sopas == null){
            return null;
        }
        char diagonal[] = new char[this.sopas.length];
        for(int i = 0; i<this.sopas.length; i++){
            diagonal[i] = this.sopas[i][i];
        }
        return  diagonal;
    }

    /**GET Method Propertie sopas*/
    public char[][] getSopas(){
        return this.sopas;
    }//end method getSopas

}
